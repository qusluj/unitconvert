#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LEN 80
#define INCHES_IN_FEET 12

int feetToInches()
{
	float numberOfFeet;
	float convertedToInches;
	
	printf("how many feet do you have?: ");
	scanf("%f", &numberOfFeet);
	convertedToInches = numberOfFeet * INCHES_IN_FEET;
	printf("that amount equals %.2f inches", convertedToInches);
	
	return 0;
}

int inchesToFeet()
{
	float numberOfInches;
	float convertedToFeet;
	
	printf("how many inches do you have?: ");
	scanf("%f", &numberOfInches);
	convertedToFeet = numberOfInches / INCHES_IN_FEET;
	printf("that amount equals %.2f inches", convertedToFeet);
	
	return 0;
}

int main (int argc, char *argv[])
{
	char unitToConvertTo[MAX_LEN]; /* the character array which holds user input */
	char letterF[15];
	char letterI[15];
	int choiceIsF; /* the function strcmp is an integer (strcmp counts differences between two strings */
	int choiceIsI;
	
	strcpy(letterF, "f"); /* strcpy appends letters to the character array */
	strcpy(letterI, "i");
	
	printf ("what unit would you like to convert to: feet or inches?f/i: ");
	scanf ("%s", unitToConvertTo);
	
	choiceIsF = strcmp(letterF, unitToConvertTo);
	choiceIsI = strcmp(letterI, unitToConvertTo);
	
	
	if (choiceIsI == 0) /* the 0 indicates that there are no differences between the two strings */
	{
		feetToInches();
	}
	else if (choiceIsF == 0)
	{
		inchesToFeet();
	}
	return 0;
}
